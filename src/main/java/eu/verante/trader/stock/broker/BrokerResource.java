package eu.verante.trader.stock.broker;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.stock.symbol.TradingSymbolCategory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface BrokerResource {
    @GetMapping("/symbol")
    List<TradingSymbol> getTradingSymbols(
            @RequestParam(name = "symbol", required = false) String symbol,
            @RequestParam(name = "category", required = false) TradingSymbolCategory category,
            @RequestParam(name = "groupName", required = false) String groupName);

    @GetMapping("/symbol/{symbol}/candles")
    List<TradingCandle> getCandles(
            @PathVariable("symbol") String symbol,
            @RequestParam("period") Period period,
            @RequestParam("from") long from);
}
