package eu.verante.trader.stock.candle;

public enum Period {
    MINUTE_1,
    MINUTE_5,
    MINUTE_15,
    MINUTE_30,
    HOUR_1,
    HOUR_4,
    DAY,
    WEEK,
    MONTH
}
