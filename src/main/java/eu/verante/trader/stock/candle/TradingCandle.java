package eu.verante.trader.stock.candle;

import lombok.Builder;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Builder
public record TradingCandle(
        String symbol,
        Period period,
        ZonedDateTime time,
        BigDecimal open,
        BigDecimal close,
        BigDecimal high,
        BigDecimal low,
        BigDecimal volume) {
}
