package eu.verante.trader.stock.symbol;

import lombok.Builder;

@Builder
public record TradingSymbol(
        String symbol,
        String description,
        String currency,
        String groupName,
        TradingSymbolCategory category) {
}
