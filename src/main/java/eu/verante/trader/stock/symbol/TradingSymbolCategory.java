package eu.verante.trader.stock.symbol;

public enum TradingSymbolCategory {
    STOCK,
    INDEX,
    FOREX,
    COMMODITY,
    ETF,
    CRYPTO
}
